var hierarchy =
[
    [ "facade.AbstractFacade< T >", "classfacade_1_1_abstract_facade.html", null ],
    [ "facade.AbstractFacade< FrogSpecies >", "classfacade_1_1_abstract_facade.html", [
      [ "facade.FrogSpeciesFacade", "classfacade_1_1_frog_species_facade.html", null ]
    ] ],
    [ "facade.AbstractFacade< Rescue >", "classfacade_1_1_abstract_facade.html", [
      [ "facade.RescueFacade", "classfacade_1_1_rescue_facade.html", null ]
    ] ],
    [ "facade.AbstractFacade< RescueData >", "classfacade_1_1_abstract_facade.html", [
      [ "facade.RescueDataFacade", "classfacade_1_1_rescue_data_facade.html", null ]
    ] ],
    [ "facade.AbstractFacade< RescueParticipationData >", "classfacade_1_1_abstract_facade.html", [
      [ "facade.RescueParticipationDataFacade", "classfacade_1_1_rescue_participation_data_facade.html", null ]
    ] ],
    [ "facade.AbstractFacade< Volunteer >", "classfacade_1_1_abstract_facade.html", [
      [ "facade.VolunteerFacade", "classfacade_1_1_volunteer_facade.html", null ]
    ] ],
    [ "utils.AuthenticationUtils", "classutils_1_1_authentication_utils.html", null ],
    [ "tests.sampleTest", "classtests_1_1sample_test.html", null ],
    [ "ScheduleEvent", null, [
      [ "model.CustomScheduleEvent", "classmodel_1_1_custom_schedule_event.html", null ]
    ] ],
    [ "Serializable", null, [
      [ "controller.AuthenticationController", "classcontroller_1_1_authentication_controller.html", null ],
      [ "controller.ScheduleBean", "classcontroller_1_1_schedule_bean.html", null ],
      [ "model.FrogSpecies", "classmodel_1_1_frog_species.html", null ],
      [ "model.Rescue", "classmodel_1_1_rescue.html", null ],
      [ "model.RescueData", "classmodel_1_1_rescue_data.html", null ],
      [ "model.RescueParticipationData", "classmodel_1_1_rescue_participation_data.html", null ],
      [ "model.Volunteer", "classmodel_1_1_volunteer.html", null ]
    ] ]
];
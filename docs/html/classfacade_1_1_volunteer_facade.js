var classfacade_1_1_volunteer_facade =
[
    [ "VolunteerFacade", "classfacade_1_1_volunteer_facade.html#a55ba2d23a75b43ab737e0aa7888e9169", null ],
    [ "findByEmail", "classfacade_1_1_volunteer_facade.html#aee05f4dcf2a1087153989b9d45b2c71c", null ],
    [ "getEntityManager", "classfacade_1_1_volunteer_facade.html#a64765aacb06c63e967cc19e38140fa57", null ],
    [ "insertVolunteer", "classfacade_1_1_volunteer_facade.html#a85d8bbe47259bca42d2d1d2dc9681360", null ],
    [ "isRoleLeader", "classfacade_1_1_volunteer_facade.html#a5a2522dee0fdcad720585074a910e996", null ],
    [ "isRoleVolunteer", "classfacade_1_1_volunteer_facade.html#a57e156692840401c9335a4a382ce71ef", null ],
    [ "roleExists", "classfacade_1_1_volunteer_facade.html#a293b6e123099e9f1526fbd138092649c", null ]
];
var classfacade_1_1_abstract_facade =
[
    [ "AbstractFacade", "classfacade_1_1_abstract_facade.html#a218f76f89c80b19c4417409532f415ce", null ],
    [ "find", "classfacade_1_1_abstract_facade.html#a1202d441aa38c4e2682455f7f09ca41d", null ],
    [ "findAll", "classfacade_1_1_abstract_facade.html#a9e4609dfb9ce5ecd3e55458b763f49d8", null ],
    [ "getEntityManager", "classfacade_1_1_abstract_facade.html#a495926c6e2e28bcb4a39831c0d91b76e", null ],
    [ "merge", "classfacade_1_1_abstract_facade.html#a5da59f367703dce9665e8995c9bcb24b", null ],
    [ "persist", "classfacade_1_1_abstract_facade.html#a1bb0cf303dd9c967d12e75f9b83aae7e", null ],
    [ "remove", "classfacade_1_1_abstract_facade.html#adc63fecd1afd028e430bd0fb29552e07", null ]
];
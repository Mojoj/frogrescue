var classmodel_1_1_rescue_data =
[
    [ "RescueData", "classmodel_1_1_rescue_data.html#af8bcb632dd46f69ec28c9d77456e606d", null ],
    [ "RescueData", "classmodel_1_1_rescue_data.html#a785062e29c65f01b61afd947c20c6d4d", null ],
    [ "getFrogSpecies", "classmodel_1_1_rescue_data.html#ab186e1de744bf22fe02009cd01840372", null ],
    [ "getId", "classmodel_1_1_rescue_data.html#a88fde27d9b9ae8174e50d5e5ef63ae8e", null ],
    [ "getQuantity", "classmodel_1_1_rescue_data.html#a7465313e651e19340ae7991cca3a4307", null ],
    [ "getRescue", "classmodel_1_1_rescue_data.html#a3fb4fdcf0920d9ae4fcfd74a6b5cde41", null ],
    [ "setFrogSpecies", "classmodel_1_1_rescue_data.html#a7c3bed934691bb8539d5ebe3eed61830", null ],
    [ "setQuantity", "classmodel_1_1_rescue_data.html#a96200a4ecec8d042d3eb40e1c7f73ae7", null ],
    [ "setRescue", "classmodel_1_1_rescue_data.html#a309657c4911989f79b1f64c1700e015a", null ]
];
var searchData=
[
  ['register_240',['register',['../classcontroller_1_1_authentication_controller.html#adad0b730e704e50b3d105c7248b79f6a',1,'controller::AuthenticationController']]],
  ['remove_241',['remove',['../classfacade_1_1_abstract_facade.html#adc63fecd1afd028e430bd0fb29552e07',1,'facade::AbstractFacade']]],
  ['rescue_242',['Rescue',['../classmodel_1_1_rescue.html#aa6f69c96d21b9dd9d8e64f86df8166aa',1,'model.Rescue.Rescue()'],['../classmodel_1_1_rescue.html#ae6f27cce5a3442556c1dca4bdf8637cb',1,'model.Rescue.Rescue(Date rescueDate, Integer applicantLimit, String city)']]],
  ['rescuedata_243',['RescueData',['../classmodel_1_1_rescue_data.html#af8bcb632dd46f69ec28c9d77456e606d',1,'model.RescueData.RescueData()'],['../classmodel_1_1_rescue_data.html#a785062e29c65f01b61afd947c20c6d4d',1,'model.RescueData.RescueData(Integer quantity)']]],
  ['rescuedatafacade_244',['RescueDataFacade',['../classfacade_1_1_rescue_data_facade.html#a6a7051e1210a73e9ffa22fb6147ab873',1,'facade::RescueDataFacade']]],
  ['rescuefacade_245',['RescueFacade',['../classfacade_1_1_rescue_facade.html#aff832a0fa0f83e62d2e7e76ef9765384',1,'facade::RescueFacade']]],
  ['rescueparticipationdata_246',['RescueParticipationData',['../classmodel_1_1_rescue_participation_data.html#a371ce3b4ab064c7b23da2fa82b11ed28',1,'model::RescueParticipationData']]],
  ['rescueparticipationdatafacade_247',['RescueParticipationDataFacade',['../classfacade_1_1_rescue_participation_data_facade.html#ad34672e51dd684c5a78e057353133011',1,'facade::RescueParticipationDataFacade']]],
  ['roleexists_248',['roleExists',['../classfacade_1_1_volunteer_facade.html#a293b6e123099e9f1526fbd138092649c',1,'facade::VolunteerFacade']]]
];

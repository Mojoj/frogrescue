var classtests_1_1sample_test =
[
    [ "sampleTest", "classtests_1_1sample_test.html#adb7c82e0b81f060c31ab8da62d9fb0ae", null ],
    [ "setup", "classtests_1_1sample_test.html#a9b93272a9f6dea6020cce46ad6faccbf", null ],
    [ "tearDown", "classtests_1_1sample_test.html#a747e80b093714767e46d63ac1ae5a08d", null ],
    [ "testIsRoleLeaderWhenVolunteerIsLeader", "classtests_1_1sample_test.html#a4ccaf82937ab4e5c1b0f392ebb70dbfe", null ],
    [ "testIsRoleLeaderWhenVolunteerIsVolunteer", "classtests_1_1sample_test.html#aaa61b0e02b67b1c6f8f6db95bd4ce91a", null ],
    [ "testIsRoleVolunteerWhenVolunteerIsLeader", "classtests_1_1sample_test.html#af78f512ff204fbc88ad4a1d80eff0e1b", null ],
    [ "testIsRoleVolunteerWhenVolunteerIsVolunteer", "classtests_1_1sample_test.html#aa6f99970d5caa11fa80a66b5580c5445", null ],
    [ "testRoleExistsWhenRoleDoesntExists", "classtests_1_1sample_test.html#a320b17345a295e1c2c49f29c4e8ed647", null ],
    [ "testRoleExistsWhenRoleExists", "classtests_1_1sample_test.html#acc3702fe5864fbf33e62b6ade41956eb", null ]
];
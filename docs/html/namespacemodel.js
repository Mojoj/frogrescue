var namespacemodel =
[
    [ "CustomScheduleEvent", "classmodel_1_1_custom_schedule_event.html", "classmodel_1_1_custom_schedule_event" ],
    [ "FrogSpecies", "classmodel_1_1_frog_species.html", "classmodel_1_1_frog_species" ],
    [ "Rescue", "classmodel_1_1_rescue.html", "classmodel_1_1_rescue" ],
    [ "RescueData", "classmodel_1_1_rescue_data.html", "classmodel_1_1_rescue_data" ],
    [ "RescueParticipationData", "classmodel_1_1_rescue_participation_data.html", "classmodel_1_1_rescue_participation_data" ],
    [ "Volunteer", "classmodel_1_1_volunteer.html", "classmodel_1_1_volunteer" ]
];
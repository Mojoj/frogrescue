var classmodel_1_1_frog_species =
[
    [ "FrogSpecies", "classmodel_1_1_frog_species.html#a908be9d4979b5506fb25337bacee4435", null ],
    [ "FrogSpecies", "classmodel_1_1_frog_species.html#a4737b06913e6f792616bcdee4b501248", null ],
    [ "getId", "classmodel_1_1_frog_species.html#a29b056ac54d0910dc23636a7bdbc01a8", null ],
    [ "getName", "classmodel_1_1_frog_species.html#abe33a0e8c435a205abce1bf1dbe247d3", null ],
    [ "getRescueDataList", "classmodel_1_1_frog_species.html#a51a72ae4ca9e124ff682821dfac0cd38", null ],
    [ "setName", "classmodel_1_1_frog_species.html#a7c72bd944416069271c212233566bbcb", null ]
];
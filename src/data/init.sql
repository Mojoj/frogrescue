/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  nzsom
 * Created: Mar 6, 2020
 */

CREATE TABLE FROG_SPECIES(ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) NOT NULL PRIMARY KEY, NAME VARCHAR2(50 CHAR) NOT NULL);

CREATE TABLE VOLUNTEER(
                  ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) NOT NULL PRIMARY KEY, 
                  VOLUNTEER_NAME VARCHAR2(70 CHAR) NOT NULL,
                  EMAIL VARCHAR2(70 CHAR) NOT NULL,
                  VOLUNTEER_PASSWORD VARCHAR2(35 CHAR) NOT NULL,
                  PHONE VARCHAR2(20 CHAR) NOT NULL,
                  VOLUNTEER_ROLE VARCHAR2(30 CHAR) DEFAULT 'Volunteer' NOT NULL);

CREATE TABLE RESCUE(
                  ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) NOT NULL PRIMARY KEY, 
                  RESCUE_DATE DATE NOT NULL,
                  LEADER NUMBER NOT NULL,
                  APPLICANT_LIMIT NUMBER NOT NULL,
                  CITY VARCHAR2(60 CHAR) NOT NULL,
                  FOREIGN KEY (LEADER) REFERENCES VOLUNTEER(ID));

CREATE TABLE RESCUE_DATA(
                  ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) NOT NULL PRIMARY KEY, 
                  RESCUE NUMBER NOT NULL,
                  FROG_SPECIES NUMBER NOT NULL,
                  QUANTITY NUMBER NOT NULL,
                  FOREIGN KEY (RESCUE) REFERENCES RESCUE(ID),
                  FOREIGN KEY (FROG_SPECIES) REFERENCES FROG_SPECIES(ID));

CREATE TABLE RESCUE_PARTICIPATION_DATA(
                  ID NUMBER GENERATED ALWAYS as IDENTITY(START with 1 INCREMENT by 1) NOT NULL PRIMARY KEY, 
                  VOLUNTEER NUMBER NOT NULL,
                  RESCUE NUMBER NOT NULL,
                  FOREIGN KEY (VOLUNTEER) REFERENCES VOLUNTEER(ID),
                  FOREIGN KEY (RESCUE) REFERENCES RESCUE(ID));
				  


ALTER TABLE RESCUE ADD "RESCUE_TITLE" VARCHAR2(30) NOT NULL;

ALTER TABLE RESCUE MODIFY id DROP IDENTITY;

CREATE sequence "RESCUE_ID_SEQ";

CREATE trigger "BI_RESCUE_ID"
  before insert on "RESCUE"
  for each row
begin
  select "RESCUE_ID_SEQ".nextval into :NEW."ID" from dual;
end;

/

CREATE sequence "VOLUNTEER_ID_SEQ";

CREATE trigger "BI_VOLUNTEER_ID"
  before insert on "VOLUNTEER"
  for each row
begin
  select "VOLUNTEER_ID_SEQ".nextval into :NEW."ID" from dual;
end;

/


CREATE sequence "RCPD_ID_SEQ";

CREATE trigger "BI_RCPD_ID"
  before insert on "RESCUE_PARTICIPATION_DATA"
  for each row
begin
  select "RCPD_ID_SEQ".nextval into :NEW."ID" from dual;
end;

/

ALTER TABLE RESCUE_PARTICIPATION_DATA drop COLUMN ID;
ALTER TABLE RESCUE_PARTICIPATION_DATA add ID Number(5,0) PRIMARY KEY;
ALTER TABLE RESCUE add IS_OPEN VARCHAR2(1 CHAR) NOT NULL;

ALTER TABLE RESCUE_DATA drop COLUMN ID;
ALTER TABLE RESCUE_DATA add ID Number(5,0) PRIMARY KEY;

CREATE sequence "RESCUE_DATA_SEQ";

CREATE trigger "BI_RC_ID"
  before insert on "RESCUE_DATA"
  for each row
begin
  select "RESCUE_DATA_SEQ".nextval into :NEW."ID" from dual;
end;

/
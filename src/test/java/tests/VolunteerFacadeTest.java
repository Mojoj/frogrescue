/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import controller.ScheduleBean;
import facade.VolunteerFacade;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Volunteer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.doReturn;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author bzolt
 */
public class VolunteerFacadeTest {


    public VolunteerFacadeTest() {
    }

    
    @Mock
    private ScheduleBean scheduleBean;

    @Mock
    private EntityManager em;
    
    @Mock
    private Query q;
    
    @InjectMocks
    private VolunteerFacade volunteerFacade;
    
    List volList;
    List leaderList;
    List sampleList;

    @Before
    public void setup() {
        volunteerFacade = new VolunteerFacade();
        volList = new ArrayList<>();
        leaderList = new ArrayList<>();
        Volunteer vol2 = new Volunteer();
        vol2.setVolunteerRole("volunteer");
        volList.add(vol2);
        Volunteer vol3 = new Volunteer();
        vol3.setVolunteerRole("leader");
        leaderList.add(vol3);
        sampleList = new ArrayList<>();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
    }


    
    @Test
    public void testIsRoleVolunteerWhenVolunteerIsVolunteer() {
        Volunteer volunteer = new Volunteer();
        volunteer.setVolunteerRole("volunteer");
        volunteer.setId(2);
        Query q = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("select v from Volunteer v where v.id = :id")).thenReturn(q);
        Mockito.when(q.setParameter("id", volunteer.getId())).thenReturn(q);
        Mockito.when(q.getSingleResult()).thenReturn(volunteer);
        boolean isVolunteer = volunteerFacade.isRoleVolunteer(volunteer);
        Assert.assertEquals(true, isVolunteer);
    }
    
    @Test
    public void testIsRoleVolunteerWhenVolunteerIsLeader() {
        Volunteer volunteer = new Volunteer();
        volunteer.setVolunteerRole("leader");
        volunteer.setId(2);
        Query q = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("select v from Volunteer v where v.id = :id")).thenReturn(q);
        Mockito.when(q.setParameter("id", volunteer.getId())).thenReturn(q);
        Mockito.when(q.getSingleResult()).thenReturn(volunteer);
        boolean isVolunteer = volunteerFacade.isRoleVolunteer(volunteer);
        Assert.assertEquals(false, isVolunteer);
    }
    
    @Test
    public void testIsRoleLeaderWhenVolunteerIsLeader() {
        Volunteer volunteer = new Volunteer();
        volunteer.setVolunteerRole("leader");
        volunteer.setId(2);
        Query q = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("select v from Volunteer v where v.id = :id")).thenReturn(q);
        Mockito.when(q.setParameter("id", volunteer.getId())).thenReturn(q);
        Mockito.when(q.getSingleResult()).thenReturn(volunteer);
        boolean isLeader = volunteerFacade.isRoleLeader(volunteer);
        Assert.assertEquals(true, isLeader);
    }
    
    @Test
    public void testIsRoleLeaderWhenVolunteerIsVolunteer() {
        Volunteer volunteer = new Volunteer();
        volunteer.setVolunteerRole("volunteer");
        volunteer.setId(2);
        Query q = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("select v from Volunteer v where v.id = :id")).thenReturn(q);
        Mockito.when(q.setParameter("id", volunteer.getId())).thenReturn(q);
        Mockito.when(q.getSingleResult()).thenReturn(volunteer);
        boolean isLeader = volunteerFacade.isRoleLeader(volunteer);
        Assert.assertEquals(false, isLeader);
    }
    
    @Test
    public void testRoleExistsWhenRoleExists() {
        String role = "volunteer"; 
        Query q = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("select v from Volunteer v where v.volunteerRole = :role")).thenReturn(q);
        Mockito.when(q.setParameter("role", role)).thenReturn(q);
        Mockito.when(q.getResultList()).thenReturn(volList);
        boolean exists = volunteerFacade.roleExists(role);
        Assert.assertEquals(true, exists);
    }
    
    @Test
    public void testRoleExistsWhenRoleDoesntExists() {
        String role = "kamurole"; 
        Query q = Mockito.mock(Query.class);
        Mockito.when(em.createQuery("select v from Volunteer v where v.volunteerRole = :role")).thenReturn(q);
        Mockito.when(q.setParameter("role", role)).thenReturn(q);
        Mockito.when(q.getResultList()).thenReturn(sampleList);
        boolean exists = volunteerFacade.roleExists(role);
        Assert.assertEquals(false, exists);
    }
}

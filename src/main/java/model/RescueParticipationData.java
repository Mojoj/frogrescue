/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bzolt
 */
@Entity
@Table(name = "RESCUE_PARTICIPATION_DATA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RescueParticipationData.findAll", query = "SELECT r FROM RescueParticipationData r")
    , @NamedQuery(name = "RescueParticipationData.findById", query = "SELECT r FROM RescueParticipationData r WHERE r.id = :id")})
public class RescueParticipationData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "RCPD_ID_SEQ", sequenceName = "RCPD_ID_SEQ")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "RCPD_ID_SEQ")  
    @Column(name = "ID")
    private BigDecimal id;
    
    @JoinColumn(name = "RESCUE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Rescue rescue;
    
    @JoinColumn(name = "VOLUNTEER", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Volunteer volunteer;

    public RescueParticipationData() {
    }

    public BigDecimal getId() {
        return id;
    }

    public Rescue getRescue() {
        return rescue;
    }

    public void setRescue(Rescue rescue) {
        this.rescue = rescue;
    }

    public Volunteer getVolunteer() {
        return volunteer;
    }

    public void setVolunteer(Volunteer volunteer) {
        this.volunteer = volunteer;
    }
    
}

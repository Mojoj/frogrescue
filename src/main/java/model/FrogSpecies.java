/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.annotation.Generated;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bzolt
 */
@Entity
@Table(name = "FROG_SPECIES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FrogSpecies.findAll", query = "SELECT f FROM FrogSpecies f")
    , @NamedQuery(name = "FrogSpecies.findById", query = "SELECT f FROM FrogSpecies f WHERE f.id = :id")
    , @NamedQuery(name = "FrogSpecies.findByName", query = "SELECT f FROM FrogSpecies f WHERE f.name = :name")})
public class FrogSpecies implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy=GenerationType.AUTO) 
    @Column(name = "ID")
    private BigDecimal id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NAME")
    private String name;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "frogSpecies", fetch = FetchType.LAZY)
    private List<RescueData> rescueDataList;

    public FrogSpecies() {
    }

    public FrogSpecies(String name) {
        this.name = name;
    }

    public BigDecimal getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public List<RescueData> getRescueDataList() {
        return rescueDataList;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FrogSpecies other = (FrogSpecies) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FrogSpecies{" + "id=" + id + '}';
    }

    
    
    
}

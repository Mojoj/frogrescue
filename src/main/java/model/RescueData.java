/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author bzolt
 */
@Entity
@Table(name = "RESCUE_DATA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RescueData.findAll", query = "SELECT r FROM RescueData r")
    , @NamedQuery(name = "RescueData.findById", query = "SELECT r FROM RescueData r WHERE r.id = :id")
    , @NamedQuery(name = "RescueData.findByRescue", query = "SELECT r FROM RescueData r WHERE r.rescue = :rescue")
    , @NamedQuery(name = "RescueData.findByQuantity", query = "SELECT r FROM RescueData r WHERE r.quantity = :quantity")})
public class RescueData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "RESCUE_DATA_SEQ", sequenceName = "RESCUE_DATA_SEQ")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "RESCUE_DATA_SEQ") 
    private BigDecimal id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "QUANTITY")
    private Integer quantity;
    
    @JoinColumn(name = "FROG_SPECIES", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private FrogSpecies frogSpecies;
    
    @JoinColumn(name = "RESCUE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Rescue rescue;

    public RescueData() {
    }

    public RescueData(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public FrogSpecies getFrogSpecies() {
        return frogSpecies;
    }

    public void setFrogSpecies(FrogSpecies frogSpecies) {
        this.frogSpecies = frogSpecies;
    }

    public Rescue getRescue() {
        return rescue;
    }

    public void setRescue(Rescue rescue) {
        this.rescue = rescue;
    }
    
}

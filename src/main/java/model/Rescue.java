/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bzolt
 */
@Entity
@Table(name = "RESCUE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rescue.findAll", query = "SELECT r FROM Rescue r")
    , @NamedQuery(name = "Rescue.findById", query = "SELECT r FROM Rescue r WHERE r.id = :id")
    , @NamedQuery(name = "Rescue.findByRescueDate", query = "SELECT r FROM Rescue r WHERE r.rescueDate = :rescueDate")
    , @NamedQuery(name = "Rescue.findByApplicantLimit", query = "SELECT r FROM Rescue r WHERE r.applicantLimit = :applicantLimit")
    , @NamedQuery(name = "Rescue.findByCity", query = "SELECT r FROM Rescue r WHERE r.city = :city")})
public class Rescue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "RESCUE_ID_SEQ", sequenceName = "RESCUE_ID_SEQ")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "RESCUE_ID_SEQ") 
    @Column(name = "ID")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "RESCUE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date rescueDate;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "APPLICANT_LIMIT")
    private Integer applicantLimit;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "CITY")
    private String city;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(name = "RESCUE_TITLE")
    private String title;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "IS_OPEN")
    private String open;
    
    @OneToMany( mappedBy = "rescue", fetch = FetchType.LAZY)
    private List<RescueData> rescueDataList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rescue", fetch = FetchType.LAZY)
    private List<RescueParticipationData> rescueParticipationDataList;
    
    @JoinColumn(name = "LEADER", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Volunteer leader;

    public Rescue() {
        this.open = "Y";
    }

    public Rescue(Date rescueDate, Integer applicantLimit, String city) {
        this.rescueDate = rescueDate;
        this.applicantLimit = applicantLimit;
        this.city = city;
        this.open = "Y";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Date getRescueDate() {
        return rescueDate;
    }

    public void setRescueDate(Date rescueDate) {
        this.rescueDate = rescueDate;
    }

    public Integer getApplicantLimit() {
        return applicantLimit;
    }

    public void setApplicantLimit(Integer applicantLimit) {
        this.applicantLimit = applicantLimit;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @XmlTransient
    public List<RescueData> getRescueDataList() {
        return rescueDataList;
    }

    public void setRescueDataList(List<RescueData> rescueDataList) {
        this.rescueDataList = rescueDataList;
    }

    @XmlTransient
    public List<RescueParticipationData> getRescueParticipationDataList() {
        return rescueParticipationDataList;
    }

    public void setRescueParticipationDataList(List<RescueParticipationData> rescueParticipationDataList) {
        this.rescueParticipationDataList = rescueParticipationDataList;
    }

    public Volunteer getLeader() {
        return leader;
    }

    public void setLeader(Volunteer leader) {
        this.leader = leader;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        if(open) {
            this.open = "Y";
        } else {
            this.open = "N";
        }
    }

    
    
}

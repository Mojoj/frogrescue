/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author bzolt
 */
@Entity
@Table(name = "VOLUNTEER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Volunteer.findAll", query = "SELECT v FROM Volunteer v")
    , @NamedQuery(name = "Volunteer.findById", query = "SELECT v FROM Volunteer v WHERE v.id = :id")
    , @NamedQuery(name = "Volunteer.findByVolunteerName", query = "SELECT v FROM Volunteer v WHERE v.volunteerName = :volunteerName")
    , @NamedQuery(name = "Volunteer.findByEmail", query = "SELECT v FROM Volunteer v WHERE v.email = :email")
    , @NamedQuery(name = "Volunteer.findByVolunteerPassword", query = "SELECT v FROM Volunteer v WHERE v.volunteerPassword = :volunteerPassword")
    , @NamedQuery(name = "Volunteer.findByPhone", query = "SELECT v FROM Volunteer v WHERE v.phone = :phone")
    , @NamedQuery(name = "Volunteer.findByVolunteerRole", query = "SELECT v FROM Volunteer v WHERE v.volunteerRole = :volunteerRole")})
public class Volunteer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator(initialValue = 1, allocationSize = 1, name = "VOLUNTEER_ID_SEQ", sequenceName = "VOLUNTEER_ID_SEQ")
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "VOLUNTEER_ID_SEQ") 
    @Column(name = "ID")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "VOLUNTEER_NAME")
    private String volunteerName;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 70)
    @Column(name = "EMAIL")
    private String email;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 35)
    @Column(name = "VOLUNTEER_PASSWORD")
    private String volunteerPassword;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "PHONE")
    private String phone;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "VOLUNTEER_ROLE")
    private String volunteerRole;
    
    @OneToMany(mappedBy = "volunteer", fetch = FetchType.LAZY)
    private List<RescueParticipationData> rescueParticipationDataList;
    
    @OneToMany(mappedBy = "leader", fetch = FetchType.LAZY)
    private List<Rescue> rescueList;

    public Volunteer() {
        rescueParticipationDataList = new ArrayList<>();
        rescueList = new ArrayList<>();
    }

    public Volunteer(String volunteerName, String email, String volunteerPassword, String phone, String volunteerRole) {
        this.volunteerName = volunteerName;
        this.email = email;
        this.volunteerPassword = volunteerPassword;
        this.phone = phone;
        this.volunteerRole = volunteerRole;
    }

    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    public String getVolunteerName() {
        return volunteerName;
    }

    public void setVolunteerName(String volunteerName) {
        this.volunteerName = volunteerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVolunteerPassword() {
        return volunteerPassword;
    }

    public void setVolunteerPassword(String volunteerPassword) {
        this.volunteerPassword = volunteerPassword;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVolunteerRole() {
        return volunteerRole;
    }

    public void setVolunteerRole(String volunteerRole) {
        this.volunteerRole = volunteerRole;
    }

    public List<RescueParticipationData> getRescueParticipationDataList() {
        return rescueParticipationDataList;
    }

    public void setRescueParticipationDataList(ArrayList<RescueParticipationData> rescueParticipationDataList) {
        this.rescueParticipationDataList = rescueParticipationDataList;
    }

    @XmlTransient
    public List<Rescue> getRescueList() {
        return rescueList;
    }

    public void setRescueList(ArrayList<Rescue> rescueList) {
        this.rescueList = rescueList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Volunteer other = (Volunteer) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Volunteer{" + "id=" + id + '}';
    }
    
    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.RescueData;
import javax.persistence.Query;
import model.FrogSpecies;
import model.Rescue;

/**
 *
 * @author bzolt
 */
@Stateless
public class RescueDataFacade extends AbstractFacade<RescueData> {

    @PersistenceContext(unitName = "com.elte_frogrescue_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RescueDataFacade() {
        super(RescueData.class);
    }

    public List<RescueData> findByRescue(Rescue rescue) {
        Query q = em.createNamedQuery("RescueData.findByRescue", RescueData.class).setParameter("rescue", rescue);
        List<RescueData> result = q.getResultList();
        return result;
    }

    public RescueData findByRescueAndFrogSpecies(Rescue rescue, FrogSpecies frogSpecies) {
        try {
            Query q = em.createQuery("select rd from RescueData rd where rd.frogSpecies = :frogSpecies and rd.rescue = :rescue").setParameter("rescue", rescue).setParameter("frogSpecies", frogSpecies);
            return (RescueData) q.getSingleResult();
        } catch (Exception ex) {
            return null;
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import model.FrogSpecies;

/**
 *
 * @author bzolt
 */
@Stateless
public class FrogSpeciesFacade extends AbstractFacade<FrogSpecies> {

    @PersistenceContext(unitName = "com.elte_frogrescue_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FrogSpeciesFacade() {
        super(FrogSpecies.class);
    }

    public FrogSpecies findById(int id){
        TypedQuery<FrogSpecies> query = em.createNamedQuery("FrogSpecies.findById", FrogSpecies.class).setParameter("id", id);
        FrogSpecies frogSpecies = null;
        try {
            frogSpecies = query.getSingleResult();
        } catch (Exception e) {
        }
        return frogSpecies;
    }

    public FrogSpecies findByName(String name) {
        TypedQuery<FrogSpecies> query = em.createNamedQuery("FrogSpecies.findByName", FrogSpecies.class).setParameter("name", name);
        FrogSpecies frogSpecies = null;
        try {
            frogSpecies = query.getSingleResult();
        } catch (Exception e) {
        }
        return frogSpecies;
    }

}

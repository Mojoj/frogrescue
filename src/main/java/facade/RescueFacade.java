/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Rescue;

/**
 *
 * @author bzolt
 */
@Stateless
public class RescueFacade extends AbstractFacade<Rescue> {

    @PersistenceContext(unitName = "com.elte_frogrescue_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RescueFacade() {
        super(Rescue.class);
    }

    public void create(Rescue rescue) {
        em.persist(rescue);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Rescue;
import model.RescueParticipationData;
import static model.RescueParticipationData_.rescue;
import model.Volunteer;

/**
 *
 * @author bzolt
 */
@Stateless
public class RescueParticipationDataFacade extends AbstractFacade<RescueParticipationData> {

    @PersistenceContext(unitName = "com.elte_frogrescue_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RescueParticipationDataFacade() {
        super(RescueParticipationData.class);
    }

    public RescueParticipationData findByRescueAndVolunteer(Rescue rescue, Volunteer volunteer) {
        try {
            Query q = em.createQuery("select rcpd from RescueParticipationData rcpd where rcpd.rescue = :rescue and rcpd.volunteer = :volunteer").setParameter("rescue", rescue).setParameter("volunteer", volunteer);
            return (RescueParticipationData) q.getSingleResult();
        }
        catch(Exception ex) {
            return null;
        }
    }

    public List<RescueParticipationData> findAllByRescue(Rescue rescue) {
        Query q = em.createQuery("select rcpd from RescueParticipationData rcpd where rcpd.rescue = :rescue").setParameter("rescue", rescue);
        return q.getResultList();
    }

    public List<RescueParticipationData> findAllByVolunteer(Volunteer volunteer) {
        Query q = em.createQuery("select rcpd from RescueParticipationData rcpd where rcpd.volunteer = :volunteer").setParameter("volunteer", volunteer);
        return q.getResultList();
    }

}

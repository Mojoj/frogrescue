/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.ejb.Stateless;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import model.Volunteer;

/**
 *
 * @author bzolt
 */
@Stateless
public class VolunteerFacade extends AbstractFacade<Volunteer> {

    @PersistenceContext(unitName = "com.elte_frogrescue_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public VolunteerFacade() {
        super(Volunteer.class);
    }

    public boolean isRoleVolunteer(Volunteer volunteer) {
        Query q = getEntityManager().createQuery("select v from Volunteer v where v.id = :id");
        q.setParameter("id", volunteer.getId());
        Volunteer vol = (Volunteer) q.getSingleResult();
        if (vol != null) {
            return vol.getVolunteerRole().equals("volunteer");
        }
        return false;
    }

    public boolean isRoleLeader(Volunteer volunteer) {
        Query q = getEntityManager().createQuery("select v from Volunteer v where v.id = :id");
        q.setParameter("id", volunteer.getId());
        Volunteer vol = (Volunteer) q.getSingleResult();
        if (vol != null) {
            return vol.getVolunteerRole().equals("leader");
        }
        return false;
    }

    public boolean roleExists(String role) {
        Query q = getEntityManager().createQuery("select v from Volunteer v where v.volunteerRole = :role");
        q.setParameter("role", role);
        List<Volunteer> vol = q.getResultList();
        if (vol != null) {
            return !vol.isEmpty();
        }
        return false;
    }

    public String insertVolunteer(Volunteer volunteer) {
        Volunteer existingVolunteer = findByEmail(volunteer.getEmail());
        System.out.println(existingVolunteer == null);

        if (existingVolunteer != null) {
            return "Email already registered.";
        }
        try {
            volunteer.setVolunteerPassword(utils.AuthenticationUtils.encodeSHA256(volunteer.getVolunteerPassword()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(volunteer.getVolunteerName());
        System.out.println(volunteer.getVolunteerPassword());
        System.out.println(volunteer.getVolunteerRole());

        em.createNativeQuery("INSERT INTO VOLUNTEER (VOLUNTEER_NAME, EMAIL, VOLUNTEER_PASSWORD, PHONE, VOLUNTEER_ROLE) VALUES (?,?,?,?,?)")
                .setParameter(1, volunteer.getVolunteerName())
                .setParameter(2, volunteer.getEmail())
                .setParameter(3, volunteer.getVolunteerPassword())
                .setParameter(4, volunteer.getPhone())
                .setParameter(5, volunteer.getVolunteerRole())
                .executeUpdate();
        //this.em.persist(volunteer);
        return null;
    }

    public void updateVolunteer(Volunteer volunteer) throws NamingException, SQLException {
        em.createNativeQuery("UPDATE VOLUNTEER SET VOLUNTEER_ROLE = ? WHERE EMAIL = ?")
                .setParameter(1, volunteer.getVolunteerRole())
                .setParameter(2, volunteer.getEmail())
                .executeUpdate();
    }
    
    public List<Volunteer> findAllVolunteer() {
        Query q = em.createQuery("Select v from Volunteer v");
        return q.getResultList();
    }

    public Volunteer findByEmail(String email) {
        TypedQuery<Volunteer> query = em.createNamedQuery("Volunteer.findByEmail", Volunteer.class).setParameter("email", email);
        Volunteer volunteer = null;
        try {
            volunteer = query.getSingleResult();
        } catch (Exception e) {
        }
        return volunteer;
    }

}

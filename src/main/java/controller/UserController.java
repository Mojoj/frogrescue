/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.VolunteerFacade;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.Volunteer;

/**
 *
 * @author bzolt /
 */
@Named(value = "userController")
@ViewScoped
public class UserController implements Serializable {

    private static final long serialVersionUID = 1685823449195612778L;

    @EJB
    VolunteerFacade volunteerFacade;

    List<Volunteer> volunteerList;

    public UserController() {

    }

    public void init() throws IOException {
        if (isUerInRole("leader")) {
            if (volunteerList == null) {
                volunteerList = volunteerFacade.findAllVolunteer();
            }
        } else {
            volunteerList = new ArrayList<>();
        }
    }

    public boolean isUerInRole(String role) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        Volunteer volunteer = (Volunteer) sessionMap.get("User");
        return volunteer.getVolunteerRole().equals(role);
    }

    public boolean isVolunteerInRole(Volunteer volunteer, String role) {
        return volunteer.getVolunteerRole().equals(role);
    }

    public List<Volunteer> getVolunteerList() {
        return volunteerList;
    }

    public void setVolunteerList(List<Volunteer> volunteerList) {
        this.volunteerList = volunteerList;
    }

    public void changeVolunteerRole(Volunteer volunteer) throws NamingException, SQLException {
        if (volunteer.getVolunteerRole().equals("leader")) {
            volunteer.setVolunteerRole("volunteer");
        } else {
            volunteer.setVolunteerRole("leader");
        }
        volunteerFacade.updateVolunteer(volunteer);
        Context ic = new InitialContext();
        DataSource ds = (DataSource) ic.lookup("java:app/ullman");
        Connection conn = ds.getConnection();
        conn.setAutoCommit(false);
        conn.commit();
        conn.setAutoCommit(true);
    }

}

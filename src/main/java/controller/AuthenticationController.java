package controller;

import facade.VolunteerFacade;
import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import model.Volunteer;
import org.primefaces.context.RequestContext;

@Named(value = "authenticationController")
@SessionScoped
public class AuthenticationController implements Serializable {

    private static final long serialVersionUID = 1685823449195612778L;
    private static Logger log = Logger.getLogger(AuthenticationController.class.getName());

    @EJB
    private VolunteerFacade volunteerFacade;

    private Volunteer volunteer;
    private Volunteer loginVolunteer;
    private String confirmPassword;

    private String script;

    public AuthenticationController() {
    }

    public void init() throws IOException {
        if (loginVolunteer != null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/index.xhtml");
        } else {
            script = "document.getElementById('loginForm:validatorMessage').style.display = 'none'";
            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("loginForm:script");
            volunteer = new Volunteer();
            loginVolunteer = null;
        }
    }

    public String register() {
        volunteer.setVolunteerRole("volunteer");
        String result = volunteerFacade.insertVolunteer(volunteer);
        if (result == null) {
            log.info(result);
            log.info("New user created");
            return "login";
        }
        log.info(result);
        return "registration";
    }

    public boolean isUerInRole(String role) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        Volunteer volunteer = (Volunteer) sessionMap.get("User");
        return volunteer.getVolunteerRole().equals(role);
    }

    public String login() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            request.login(volunteer.getEmail(), volunteer.getVolunteerPassword());
        } catch (ServletException e) {
            script = "document.getElementById('loginForm:validatorMessage').style.display = 'flex'";
            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("loginForm:script");
            return "login";
        }
        Principal principal = request.getUserPrincipal();
        loginVolunteer = volunteerFacade.findByEmail(principal.getName());
        log.info("Authentication done for user: " + principal.getName());
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        sessionMap.put("User", loginVolunteer);
        if (request.isUserInRole("users")) {
            System.out.println("IS IN");
            FacesContext.getCurrentInstance().getExternalContext().redirect("register.xhtml");
            return "index";
        } else {
            System.out.println("NOT IN");
            FacesContext.getCurrentInstance().getExternalContext().redirect("faces/index.xhtml");
            return "index";
        }
    }

    public void logout() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        try {
            loginVolunteer = null;
            request.logout();
            // clear the session
            ((HttpSession) context.getExternalContext().getSession(false)).invalidate();
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            Map<String, Object> sessionMap = externalContext.getSessionMap();
            sessionMap.remove("User");
            FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
        } catch (ServletException e) {
            log.log(Level.SEVERE, "Failed to logout user!", e);
        }
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Volunteer getUser() {
        return volunteer;
    }

    public void setUser(Volunteer user) {
        this.volunteer = user;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public Volunteer getLoginUser() {
        return volunteer;
    }

    public void setLoginUser(Volunteer loginUser) {
        this.volunteer = loginUser;
    }

}

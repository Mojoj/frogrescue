/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import facade.RescueFacade;
import facade.RescueParticipationDataFacade;
import facade.VolunteerFacade;
import facade.FrogSpeciesFacade;
import facade.RescueDataFacade;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import model.CustomScheduleEvent;
import model.Rescue;
import model.RescueParticipationData;
import model.Volunteer;
import model.FrogSpecies;
import model.RescueData;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

/**
 *
 * @author Fruzsina
 */
@Named
@ViewScoped
public class ScheduleBean implements Serializable {

    public static long serialNumber = 1L;

    private int rescueDataFrogQuantity = 0;

    @EJB
    VolunteerFacade volunteerFacade;
    @EJB
    RescueFacade rescueFacade;
    @EJB
    RescueParticipationDataFacade rcpdFacade;
    @EJB
    FrogSpeciesFacade frogFacade;
    @EJB
    RescueDataFacade rescueDataFacade;

    private List<Volunteer> volunteerList;
    private List<ScheduleEvent> scheduleEvents;
    private List<Rescue> rescueList;
    private List<FrogSpecies> speciesList;
    private List<RescueData> rescueDataList;

    private ScheduleModel model;
    private ScheduleEvent event;
    private Rescue rescue;
    private RescueData rescueData;

    public ScheduleBean() {
        model = new DefaultScheduleModel();
        event = new CustomScheduleEvent();
        rescue = new Rescue();
        rescueData = new RescueData();
        rescueList = new ArrayList<>();
        volunteerList = new ArrayList<>();
        speciesList = new ArrayList<>();
        rescueDataList = new ArrayList<>();
    }

    public void init() {
        volunteerList = volunteerFacade.findAllVolunteer();
        speciesList = frogFacade.findAll();
        if (this.model != null) {
            if (this.scheduleEvents == null) {
                this.scheduleEvents = new ArrayList<>();
            }
            rescueList = rescueFacade.findAll();
            if (rescueList != null) {
                for (Rescue rescue : rescueList) {
                    Date dt = datePlusOne(rescue.getRescueDate());
                    ScheduleEvent newEvent = new CustomScheduleEvent(rescue.getTitle(), dt, dt, true, rescue);
                    if (!this.scheduleEvents.contains(newEvent)) {
                        newEvent.setId(rescue.getId().toString());
                        this.scheduleEvents.add(newEvent);
                        this.model.addEvent(newEvent);
                    }
                }
            }
        }
    }

    public void save() throws NamingException, SQLException {
        ScheduleEvent newEvent = new CustomScheduleEvent(rescue.getTitle(), rescue.getRescueDate(), rescue.getRescueDate(), true, this.rescue);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        Volunteer loggedInVolunteer = (Volunteer) sessionMap.get("User");
        rescue.setLeader(loggedInVolunteer);
        if (rescue.getId() == null) {
            rescueFacade.create(rescue);
            Context ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("java:app/ullman");
            Connection conn = ds.getConnection();
            conn.setAutoCommit(false);
            conn.commit();
            conn.setAutoCommit(true);
            Date dt = datePlusOne(newEvent.getStartDate());
            ScheduleEvent newEventPlusOne = new CustomScheduleEvent(rescue.getTitle(), dt, dt, true, this.rescue);
            model.addEvent(newEventPlusOne);
        } else {
            newEvent.setId(event.getId());
            rescueFacade.merge(rescue);
            Date dt = datePlusOne(newEvent.getStartDate());
            ScheduleEvent newEventPlusOne = new CustomScheduleEvent(rescue.getTitle(), dt, dt, true, this.rescue);
            for (RescueData rd : rescueDataList) {
                if (rescueDataFacade.findByRescueAndFrogSpecies(rd.getRescue(), rd.getFrogSpecies()) == null) {
                    rescueDataFacade.persist(rd);
                }
            }
            model.updateEvent(newEventPlusOne);
        }
        addMessage("Event added");
    }

    public void addToRescueDataList() {
        RescueData temp = new RescueData();
        temp.setRescue(rescue);
        rescueDataList.add(temp);
    }

    public void removeFromDataList(int index) {
        rescueDataList.remove(index);
    }

    public void delete() throws IOException {
        model.deleteEvent(event);
        List<RescueData> temp = rescueDataFacade.findByRescue(rescue);
        if (temp.size() > 0) {
            for (RescueData rd : temp) {
                rescueDataFacade.remove(rd);
            }
        }
        List<RescueParticipationData> tempData = rcpdFacade.findAllByRescue(rescue);
        if (tempData.size() > 0) {
            for (RescueParticipationData rcpd : tempData) {
                rcpdFacade.remove(rcpd);
            }
        }

        rescueList.remove(rescue);
        rescueFacade.remove(this.rescue);
        scheduleEvents.remove(event);
        addMessage("Delete successful");
    }

    private void addMessage(String message) {
        FacesMessage msg = new FacesMessage(message);
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void registerForFrogRescue(Rescue rescue) throws NamingException, SQLException {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        Volunteer volunteer = (Volunteer) sessionMap.get("User");
        RescueParticipationData rcpd = rcpdFacade.findByRescueAndVolunteer(rescue, volunteer);
        if (getApplicantCountForRescue(rescue) < rescue.getApplicantLimit() && rcpd == null) {
            RescueParticipationData rcpData = new RescueParticipationData();
            rcpData.setVolunteer(volunteer);
            rcpData.setRescue(rescue);
            rcpdFacade.persist(rcpData);
            Context ic = new InitialContext();
            DataSource ds = (DataSource) ic.lookup("java:app/ullman");
            Connection conn = ds.getConnection();
            conn.setAutoCommit(false);
            conn.commit();
            conn.setAutoCommit(true);
            RescueParticipationData newRcpData = rcpdFacade.findByRescueAndVolunteer(rescue, volunteer);
            volunteer.getRescueParticipationDataList().add(newRcpData);
            addMessage("Sikeres jelentkezés");
        } else {
            if (getApplicantCountForRescue(rescue) >= rescue.getApplicantLimit()) {
                addMessage("Ez a békamentés esemény már betelt! Jelentkezés sikertelen");
            } else {
                addMessage("Már jelentkeztél erre az eseményre");
            }
        }

    }

    public String formattedDateInString(Date date) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
            return formatter.format(date);
        }
        return "";
    }

    public Integer getApplicantCountForRescue(Rescue rescue) {
        return rcpdFacade.findAllByRescue(rescue).size();
    }

    public boolean isUerInRole(String role) {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map<String, Object> sessionMap = externalContext.getSessionMap();
        Volunteer volunteer = (Volunteer) sessionMap.get("User");
        return volunteer.getVolunteerRole().equals(role);
    }

    private Date datePlusOne(Date date) {
        Date dt = date;
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        dt = c.getTime();
        return dt;
    }

    public void close() throws NamingException, SQLException {
        this.rescue.setOpen(false);
        rescueFacade.merge(rescue);
        addMessage("Closing successful");
    }

    public int findAllByRescue() {
        return rcpdFacade.findAllByRescue(rescue).size();
    }

    public Boolean getRescueClosed() {
        return (this.rescue.getOpen().equals("N"));
    }

    public Boolean getRescueOpen() {
        return (this.rescue.getOpen().equals("Y"));
    }

    public ScheduleModel getModel() {
        return model;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public void onEventSelect(SelectEvent e) {
        event = (CustomScheduleEvent) e.getObject();
        this.rescue = (Rescue) event.getData();
        if (this.rescue.getId() != null) {
            rescueDataList = rescueDataFacade.findByRescue(this.rescue);
        }
    }

    public void onDateSelect(SelectEvent e) {
        this.rescue = new Rescue();
        Date date = (Date) e.getObject();
        this.rescue.setRescueDate(date);
        this.rescue.setRescueDate(date);
        rescueDataList = new ArrayList<>();
    }

    public boolean isRoleVolunteer(Volunteer volunteer) {
        return volunteerFacade.isRoleVolunteer(volunteer);
    }

    public List<ScheduleEvent> getScheduleEvents() {
        return scheduleEvents;
    }

    public void setScheduleEvents(List<ScheduleEvent> scheduleEvents) {
        this.scheduleEvents = scheduleEvents;
    }

    public List<Volunteer> getVolunteerList() {
        return volunteerList;
    }

    public void setVolunteerList(List<Volunteer> volunteerList) {
        this.volunteerList = volunteerList;
    }

    public List<Rescue> getRescueList() {
        return rescueList;
    }

    public void setRescueList(List<Rescue> rescueList) {
        this.rescueList = rescueList;
    }

    public Rescue getRescue() {
        return rescue;
    }

    public void setRescue(Rescue rescue) {
        this.rescue = rescue;
    }

    public List<FrogSpecies> getSpeciesList() {
        return speciesList;
    }

    public void setSpeciesList(List<FrogSpecies> speciesList) {
        this.speciesList = speciesList;
    }

    public FrogSpecies getFrog(int id) {
        return speciesList.get(id);
    }

    public RescueData getRescueData() {
        return rescueData;
    }

    public void setRescueData(RescueData rescueData) {
        this.rescueData = rescueData;
    }

    public List<RescueData> getRescueDataList() {
        return rescueDataList;
    }

    public void setRescueDataList(List<RescueData> rescueDataList) {
        this.rescueDataList = rescueDataList;
    }

    public int getRescueDataFrogQuantity() {
        return rescueDataFrogQuantity;
    }

    public void setRescueDataFrogQuantity(int rescueDataFrogQuantity) {
        this.rescueDataFrogQuantity = rescueDataFrogQuantity;
    }

}

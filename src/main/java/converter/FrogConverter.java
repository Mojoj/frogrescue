package converter;

import facade.FrogSpeciesFacade;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import model.FrogSpecies;

@FacesConverter(value = "frogConverter")
public class FrogConverter implements Converter{
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return null;
        }
        FrogSpecies frogSpecies = (FrogSpecies) value;
        return frogSpecies.getName();
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
                return null;
        }
        //Integer id = Integer.valueOf(value);
        FrogSpecies frogSpecies = null;
        try {
            FrogSpeciesFacade frogSpeciesFacade = (FrogSpeciesFacade) new InitialContext().lookup("java:module/FrogSpeciesFacade");
            frogSpecies = frogSpeciesFacade.findByName(value);
        } catch (NamingException ex) {
            Logger.getLogger(FrogConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return frogSpecies;
    }
}